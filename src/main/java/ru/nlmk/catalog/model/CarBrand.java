package ru.nlmk.catalog.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Table(name = "carbrand", schema = "public")
public class CarBrand {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_carbrand_id")
    @SequenceGenerator(name = "seq_carbrand_id", sequenceName = "seq_carbrand_id", schema = "public", allocationSize = 1)
    private Long id;

    @Column(nullable = false)
    private String name;

}
