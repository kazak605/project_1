package ru.nlmk.catalog.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Table(name = "carmodel", schema = "public")
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_carmodel_id")
    @SequenceGenerator(name = "seq_carmodel_id", sequenceName = "seq_carmodel_id", schema = "public", allocationSize = 1)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "brand_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private CarBrand carBrand;

    @Column(nullable = false)
    private String name;

}
