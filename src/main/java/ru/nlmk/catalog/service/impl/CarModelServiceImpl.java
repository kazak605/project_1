package ru.nlmk.catalog.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.catalog.dto.CarModelDTO;
import ru.nlmk.catalog.dto.mapper.CarModelDTOMapper;
import ru.nlmk.catalog.repository.CarModelRepository;
import ru.nlmk.catalog.service.CarModelService;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CarModelServiceImpl implements CarModelService {

    private CarModelRepository carModelRepository;

    private CarModelDTOMapper carModelDTOMapper;

    @Autowired
    public void setCarModelRepository(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }

    @Autowired
    public void setCarModelDTOMapper(CarModelDTOMapper carModelDTOMapper) {
        this.carModelDTOMapper = carModelDTOMapper;
    }

    @Override
    public List<CarModelDTO> findByBrandId(final Long brandId) {
        return carModelRepository.findByBrandId(brandId).stream().map(carModelDTOMapper::toDto).collect(Collectors.toList());
    }

}
