package ru.nlmk.catalog.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.catalog.dto.CarBrandDTO;
import ru.nlmk.catalog.dto.mapper.CarBrandDTOMapper;
import ru.nlmk.catalog.model.CarBrand;
import ru.nlmk.catalog.repository.CarBrandRepository;
import ru.nlmk.catalog.service.CarBrandService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Log4j2
@Service
public class CarBrandServiceImpl implements CarBrandService {

    private CarBrandRepository carBrandRepository;

    private CarBrandDTOMapper carBrandDTOMapper;

    @Autowired
    public void setCarBrandRepository(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Autowired
    public void setCarBrandDTOMapper(CarBrandDTOMapper carBrandDTOMapper) {
        this.carBrandDTOMapper = carBrandDTOMapper;
    }

    @Override
    public List<CarBrandDTO> findAll() {
        return StreamSupport
                .stream(carBrandRepository.findAll().spliterator(), false)
                .sorted(Comparator.comparing(CarBrand::getName))
                .map(carBrandDTOMapper::toDto)
                .collect(Collectors.toList());
    }

}
