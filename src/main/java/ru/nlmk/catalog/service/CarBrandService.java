package ru.nlmk.catalog.service;

import ru.nlmk.catalog.dto.CarBrandDTO;

import java.util.List;

public interface CarBrandService {

    List<CarBrandDTO> findAll();

}
