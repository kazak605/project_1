package ru.nlmk.catalog.service;

import ru.nlmk.catalog.dto.CarModelDTO;

import java.util.List;

public interface CarModelService {

    List<CarModelDTO> findByBrandId(final Long brandId);

}
