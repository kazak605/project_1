package ru.nlmk.catalog.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarModelDTO implements Serializable {

    private Long id;
    private Long brandId;
    private String name;

}
