package ru.nlmk.catalog.dto.mapper;

import ru.nlmk.catalog.dto.CarModelDTO;
import ru.nlmk.catalog.model.CarModel;

public interface CarModelDTOMapper {

    CarModelDTO toDto(CarModel carModel);

}
