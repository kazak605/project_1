package ru.nlmk.catalog.dto.mapper;

import ru.nlmk.catalog.dto.CarBrandDTO;
import ru.nlmk.catalog.model.CarBrand;

public interface CarBrandDTOMapper {

    CarBrandDTO toDto(CarBrand carBrand);
}
