package ru.nlmk.catalog.dto.mapper.impl;

import org.springframework.stereotype.Component;
import ru.nlmk.catalog.dto.CarModelDTO;
import ru.nlmk.catalog.dto.mapper.CarModelDTOMapper;
import ru.nlmk.catalog.model.CarModel;

@Component
public class CarModelDTOMapperImpl implements CarModelDTOMapper {

    @Override
    public CarModelDTO toDto(CarModel carModel) {
        return CarModelDTO.builder()
                .id(carModel.getId())
                .name(carModel.getName())
                .brandId(carModel.getCarBrand() == null ? null : carModel.getCarBrand().getId())
                .build();
    }

}
