package ru.nlmk.catalog.dto.mapper.impl;

import org.springframework.stereotype.Component;
import ru.nlmk.catalog.dto.CarBrandDTO;
import ru.nlmk.catalog.model.CarBrand;

@Component
public class CarBrandDTOMapperImpl implements ru.nlmk.catalog.dto.mapper.CarBrandDTOMapper {

    @Override
    public CarBrandDTO toDto(CarBrand carBrand) {
        return CarBrandDTO.builder()
                .id(carBrand.getId())
                .name(carBrand.getName())
                .build();
    }

}
