package ru.nlmk.catalog.controller;

import org.springframework.http.ResponseEntity;
import ru.nlmk.catalog.dto.CarModelDTO;

import java.util.List;

public interface CarModelController {

    ResponseEntity<List<CarModelDTO>> getModelsByBrandId(Long brandId);

}
