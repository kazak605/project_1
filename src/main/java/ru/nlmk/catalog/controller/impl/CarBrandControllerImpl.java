package ru.nlmk.catalog.controller.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.catalog.controller.CarBrandController;
import ru.nlmk.catalog.dto.CarBrandDTO;
import ru.nlmk.catalog.service.CarBrandService;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/brands")
public class CarBrandControllerImpl implements CarBrandController {

    private final CarBrandService carBrandService;

    @Autowired
    public CarBrandControllerImpl(CarBrandService carBrandService) {
        this.carBrandService = carBrandService;
    }


    @Override
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<CarBrandDTO>> getAllBrands() {
        log.info("Find brands");
        return ResponseEntity.ok(carBrandService.findAll());
    }

}
