package ru.nlmk.catalog.controller.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.catalog.controller.CarModelController;
import ru.nlmk.catalog.dto.CarModelDTO;
import ru.nlmk.catalog.service.CarModelService;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/models")
public class CarModelControllerImpl implements CarModelController {

    private final CarModelService carModelService;

    @Autowired
    public CarModelControllerImpl(CarModelService carModelService) {
        this.carModelService = carModelService;
    }


    @Override
    @GetMapping(value = "/{brandId}", produces = "application/json")
    public ResponseEntity<List<CarModelDTO>> getModelsByBrandId(@PathVariable Long brandId) {
        log.info("Find models by brand {}", brandId);
        List<CarModelDTO> carModelList = carModelService.findByBrandId(brandId);
        if (carModelList.isEmpty()) {
            return new ResponseEntity(carModelList, HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(carModelList);
    }
}
