package ru.nlmk.catalog.controller;

import org.springframework.http.ResponseEntity;
import ru.nlmk.catalog.dto.CarBrandDTO;

import java.util.List;

public interface CarBrandController {

    ResponseEntity<List<CarBrandDTO>> getAllBrands();

}
