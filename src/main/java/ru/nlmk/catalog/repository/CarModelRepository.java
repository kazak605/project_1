package ru.nlmk.catalog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nlmk.catalog.model.CarModel;

import java.util.List;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {

    @Query("SELECT u FROM CarModel as u WHERE u.carBrand.id = :id")
    List<CarModel> findByBrandId(@Param("id") Long brandId);

}
