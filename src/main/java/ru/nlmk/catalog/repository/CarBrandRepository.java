package ru.nlmk.catalog.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nlmk.catalog.model.CarBrand;

public interface CarBrandRepository extends CrudRepository<CarBrand, Long> {

}
