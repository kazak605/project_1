# Car Catalog Service

#### 1. Информация о проекте

Агрегатор справочников автомоболей

#### 2. Используемый стек технологий

- Java 11;
- Apache Maven.

#### 3. Сборка проекта
```cmd
mvn spring-boot:build-image
```
